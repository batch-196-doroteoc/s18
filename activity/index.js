// console.log("Hello World");

//1.

function addNumbers(number1,number2){
	let sum = number1 + number2;
	console.log("Displayed sum of "+ number1 + " and " + number2 +":")
	console.log(sum);
};
addNumbers(5,15);
;

//2.
function subtractNumbers(number1,number2){
	let difference = number1 - number2;
	console.log("Displayed difference of "+ number1 + " and " + number2 +":")
	console.log(difference);
};
subtractNumbers(20,5);

//3.
function multiplyNumbers(number1,number2){
 
console.log("The product of "+ number1 + " and "+ number2 + ":")
 return number1 * number2
};

let product = multiplyNumbers(50,10);
console.log(product);

//4.
function divideNumbers(number1,number2){
 
console.log("The quotient of "+ number1 + " and "+ number2 + ":")
 return number1 / number2
};

let quotient = divideNumbers(50,10);
console.log(quotient);

//5.
function getAreaOfCircle(number){
	console.log("The result of getting the area of a circle with " + number + " radius:")
	return (number*number)*3.1416
};
let circleArea = getAreaOfCircle(15);
console.log(circleArea);

//6.
function getAverageOfFourNumbers(number1,number2,number3,number4){
	console.log("The average of " + number1 + ", " + number2 + ", " + number3 + ", " + number4 + ":")
	return (number1+number2+number3+number4)/4
}
let averageVar = getAverageOfFourNumbers(20,40,60,80);
console.log(averageVar);

//7.

function checkPercentageAgainstPassingPercentage (number1,number2) {
	console.log("Is " + number1+ "\/" + number2 + " a passing score?")
	let percentage = ((number1/number2)*100);	
	let isPassed = percentage>=75;
	return isPassed
}

let isPassed = checkPercentageAgainstPassingPercentage(38,50);
console.log(isPassed);

