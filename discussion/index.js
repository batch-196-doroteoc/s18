//Function able to receive data without the use of global variables or prompt();

//name is the parameter
//A paramter is a variable/containers that exists only in our function and is used to store information that is provided to a function when it is called/invoked.
function printName(name){
	console.log("My name is " + name);
};

// inenvoke tinwag printName
//Data passed into a function invocation be received by the function
//This is what we call an argument
//Juana and Jimin are arguments
printName("Juana");
printName("Jimin");

//Data passed into the function through function invocation is called arguments
//The argument is then stored within a container called a parameter

function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge( );

// console.log(age);
//check divisibility reusably using a function with arguments and parameters
function checkDivisibiltyBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log ("Is " + num + " divisble by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibiltyBy8(64);
checkDivisibiltyBy8(27);


/*
	Mini-Activity

	*/


function printMyFavoriteSuperHero(favoriteSuperHero){
	console.log("My favorite superhero is " + favoriteSuperHero + ".");
};

printMyFavoriteSuperHero("Deadpool");

function printMyFavoriteLanguage(language){
	console.log("My favorite language is: " + language + ".");
}

printMyFavoriteLanguage("Javascript");
printMyFavoriteLanguage("Java");

//Multiple arguments can also be passed into a function; multiple parameters can contain our arguments

function printFullName (firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " +lastName);
}

printFullName("Juan", "Crisostomo", "Ibarra");
printFullName("Camille","Balbuena","Doroteo");
// printFullName("Jimin", " ", "Park");
// printFullName("Dahyun", "Kim", "Dela Cruz");

/*
	Paramters will contain the arguments according to the order it was passed

	"Juan" - firstName
	"Crisostomo" - middleName
	"Ibarra" = lastName

	In other langauges, providing more/less arguments than the expected paramters sometimes causes an error or changes the behavior of the functtion.

	In Javascript, we don't have to worry about that.

	In Javascript, providing less arguments than the expected paramters will automatically assign an underfined value to the parameter

 */

printFullName("Stephen","Wardell")
printFullName("Stephen","Wardell","Curry")
//commas are used to separate values
printFullName("Stephen","Wardell","Curry","James")
// printFullName("","","Me");

//Use Variables as arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName,mName,lName);

function printFavoriteSongs(faveSong1,faveSong2,faveSong3,faveSong4,faveSong5){

	console.log("These are my favorite songs: " + faveSong1 + ", " + faveSong2+ ", " + faveSong3 + ", " +faveSong4 + ", " + faveSong5)
};

printFavoriteSongs("Spring Day", "Coffee", "Just Dance","All the Love in the World", "Open Arms");

function printMyTop5Songs(song1,song2, song3, song4, song5){
	console.log("These are my favorite songs:");
	console.log(song1);
	console.log(song2);
	console.log(song3);
	console.log(song4);
	console.log(song5);

	// return song1 + song2 + song3 + song4 + song5;
	// return "hello, ma'am tine";
}

let sample1 = printMyTop5Songs("Spring Day", "Coffee", "Just Dance","All the Love in the World", "Open Arms");
console.log(sample1);


//Return Statement
//Currently or so far, our functions are able to display data in our console
//However, our functions cannot yet return values. Functions are able to return values which can be saved into a variable using the return statement/keyword

let fullName = printFullName ("Mark", "Joseph", "Lacdao")
console.log(fullName); //undefined
//print full name does not return a value
//it prints sa console

function returnFullName(firstName, middleName, lastName){

		return firstName + " " + middleName + " " + lastName;
}

fullName = returnFullName("Ernesto", "Antonio", "Maceda");
// printFullName(); //return undefined because the function does not have a return statement
// Let booleanSample = 1 === 1;
console.log(fullName);

//functions which have a return statement are able to return value and it can be saved in a variable.

console.log(fullName + " is my grandpa.");


//console log print, return=return the value and saved as value

function returnPhilippineAddress(city){

	return city + ", Philippines"

}

let myFullAddress = returnPhilippineAddress("Cainta");
console.log(myFullAddress);

//return a value from a function which we can save in a variable


function checkDivisibiltyBy4(num){

	let remainder = num % 4;

	let isDivisibleBy4 = remainder===0;

	//return either true or false
	//not only can you return raw values or data, you can also directly return a variable
	return isDivisibleBy4;
	//return keyword not only allows us to return value but also ends the process of our function
	console.log("I am run after the return")//the process ended in the first return, so hindi nagana itong console log na ito
	//pag nagreturn tapos na
	//pag null wala, di error
	//hindi error kung walang nahanap
}

let num4isDivisibleBy4 = checkDivisibiltyBy4(4);
let num14isDivisibleBy4 = checkDivisibiltyBy4(14);

console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);


function createPlayerInfo(username,level,job){

	return "username: " + username + ", level: " + level + ", job: " + job
};

let user1 = createPlayerInfo("white_night",95,"Paladin");
console.log(user1);

